var links = document.querySelectorAll(".link");
var result = document.querySelector("#result");
var clickCount = 0;

function keyDown(e) {
    console.log('KEYDOWN key: '+e.key);
    console.log('KEYDOWN code: '+e.keyCode);
}

document.body.addEventListener("keydown", keyDown)

result.addEventListener("mouseover", resultOver)

for(i = 0; i< links.length; i++) {
    links[i].addEventListener("click", linkClicked);
}

function linkClicked(event) {
    clickCount ++;
    result.innerHTML = "CLICKED "+clickCount;
}

function resultOver(event){
    result.innerHTML = "";
}
var posLeft = 100;
var posTop = 100;
var speed = 10;

var dog = document.getElementById("dog");
dog.style = "left:"+posLeft+"px;top:"+posTop+"px;";

document.body.addEventListener('keydown', keyDowned);

function keyDowned(e){
    switch (e.key) {
        case "ArrowRight" :
            posLeft += speed;
            break;
        case "ArrowLeft" :
            posLeft-= speed;
            break;
        case "ArrowUp" :
            posTop-= speed;
            break;
        case "ArrowDown" :
            posTop+= speed;
            break;

    }
    dog.style = "left:"+posLeft+"px;top:"+posTop+"px;";

    console.log("key down : " + e.key);
}
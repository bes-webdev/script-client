var interval = setInterval(clock,1000 / 60);
var showImage = true;
surpriseTimeout();


function clock(){
    var date = new Date();
    var hourString = date.getHours() + ':' + date.getMinutes() + ':'+ date.getSeconds() + ':'+ date.getMilliseconds();
    document.getElementById('clock').textContent = hourString;
}


function surpriseTimeout(){
    // setTimeout(surprise, 5000);
    var plusMilli = randomMilli();
    var nextSurpriseDate = new Date();
    nextSurpriseDate.setMilliseconds(nextSurpriseDate.getMilliseconds() + plusMilli);
    document.getElementById('nextSurprise').textContent = nextSurpriseDate.getHours() + ':' + nextSurpriseDate.getMinutes() + ':'+ nextSurpriseDate.getSeconds() + ':'+ nextSurpriseDate.getMilliseconds();
    setTimeout(surprise, plusMilli);
}

function surprise() {
    var displayValue = 'none';
    if (showImage){
        displayValue = 'block';
    }
    // displayValue = showImage ? 'block' : 'none'; // Condition ternaire équivalent à celle au dessus
    document.getElementById('surprise').style.display = displayValue;

    if (showImage === true){
        showImage = false;
    }
    else{
        showImage = true;
    }
    // showImage = !showImage; // équivalent à la condition au dessus
    surpriseTimeout();
}

function randomMilli(){
    var max = 10000;
    var min = 5000;
    var rand =  Math.random() * (max - min) + min;
    return rand;
}
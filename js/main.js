
//Strings
var myString = 'toto';
console.log(myString.toUpperCase()); //toUpperCase est une methode du type String

//Numbers
var myNumber = 4;
var myNumberAsString = '4';
if (myNumber === myNumberAsString) { // Triple egal compare la valeur mais aussi le type.
    console.log('TRUE');
} else {
    console.log('FALSE');
}

var myBoolean = true;
//Arrays
var myArray = ['a','b','c','d'];
showArrayContent(myArray)
myArray.push('e'); // Push est une methode du type array
showArrayContent(myArray)


function showArrayContent(arrayArgument){
    for(var i = 0; i<arrayArgument.length; i++){ // length est une propriété du type array
        console.warn(arrayArgument[i]);
    }
}

var isMyArrayFilled = myArray.length ? true : false; // Condition ternaire

//Objects
var myObject = {
    date: "2019-11-05", // une propriété représente une valeur propre à l'object
    color: "red",
    name: "Toto",
    dateAndColor: function(){ // Une méthde représente une function (action) propre au type de l'object
        console.log("dateAndColor");
    }
};

console.log(myObject.name);
myObject.dateAndColor();


//Functions
var roomSurface = multiply(7.5,6.3);

function multiply(val1, val2){
    console.info('enter multiply function');
    var result = val1 * val2;
    console.info('exit multiply function');
    return result; // c'est comme si la function avait la valeur de ce qu'elle retourne
    // Aucune commande écrite après un return ne s'executera, la function s'arrête au return.
}




var navList = document.getElementById("navList");
navList.style = "background: red";
navList.href = "https://reddit.be";

document.body.style = "margin:0";

var homeLink = document.getElementById("homeLink");
homeLink.href = "https://reddit.com";
console.log(homeLink.getAttribute('href'));
homeLink.setAttribute('href','https://google.be');
console.log(homeLink.getAttribute('href'));

console.log("Largeur fenêtre : "+ window.innerWidth);
console.log("Largeur document : "+ document.body.clientWidth);
console.log("Hauteur fenêtre : "+ window.innerHeight);
console.log("Hauteur document : "+ document.body.clientHeight);
console.log("url : "+ window.location);

var liList = document.getElementsByTagName("li");
console.log(liList);


//Récupérer l'html de tous les liens de mon menu
var links = document.querySelectorAll("#menu a");

for(var i =0; i<links.length; i++){
    console.log('Link '+i);
    console.log(links[i].innerHTML);
    links[i].innerHTML = links[i].innerHTML + i; //innerHTML a comme valeur une chaine de charactères représentant le contenu d'une balise, balises enfants comprises
    console.log(links[i].textContent); // textContent est comme innerHTML mais sans les balises enfants
}

// string.split() sépare un string en élements d'un tableau
// array.join() rassemble les élements d'un tableau en un string
// Exemple avec className
console.log('classes de homeLink : '+homeLink.className);
//Tableau de classes
var homeLinkClasses = homeLink.className.split(' ');
console.log(homeLinkClasses);
console.log(homeLink.classList);

homeLink.classList.add("newClass");
console.log('classes de homeLink : '+homeLink.className);